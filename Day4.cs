﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace AdventOfCode
{


    class Day4
    {
        private List<string> valid = new List<string>() {
         "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980 hcl:#623a2f",
         "eyr:2029 ecl:blu cid:129 byr:1989 iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm",
         "hcl:#888785 hgt:164cm byr:2001 iyr:2015 cid:88 pid:545766238 ecl:hzl eyr:2022",
         "iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"};


        private List<string> invalid = new List<string>() {
            "eyr:1972 cid:100 hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926",
            "iyr:2019 hcl:#602927 eyr:1967 hgt:170cm ecl:grn pid:012533040 byr:1946",
            "hcl:dab227 iyr:2012 ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277",
            "hgt:59cm ecl:zzz eyr:2038 hcl:74454a iyr:2023 pid:3556412378 byr:2007"
        };

        public int GetResult()
        {
            var results = 0;
            foreach (string line in GetLines())
            {
                int keys = 0;
                foreach (char c in line.ToCharArray())
                {
                    if (c == ':')
                        keys++;
                }

                if (keys == 8 || (keys == 7 && !line.Contains("cid:")))
                    results++;
            }
            return results;
        }


        private List<string> GetLines()
        {
            var lines = File.ReadAllLines("Input/day4.txt", Encoding.UTF8);
            List<string> result = new List<string>();
            string batch = string.Empty;
            foreach (var data in lines)
            {
                if (!string.IsNullOrWhiteSpace(data))
                {
                    batch += data + " ";
                    continue;
                } else {
                    result.Add(batch.Trim());
                    batch = string.Empty;
                }
            }
            // add last line as well
            result.Add(batch.Trim());
            
            return result;
        }

        public long GetStar()
        {

            var results = 0;
            foreach (string line in GetLines())
            {
                string[] entries = line.Split(' ');

                int matches = 0;
                foreach (string item in entries)
                {
                    var itm = item.Split(':');
                    ItemType t = (ItemType)Enum.Parse(typeof(ItemType), itm[0], true);
                    if(t.Equals(ItemType.cid))
                        continue;
                        
                    KeyValuePair<ItemType, string> entry = new KeyValuePair<ItemType, string>(t, itm[1]);
                    if (!isItemValid(entry))
                        break;

                    matches++;
                }
                if (matches == 7)
                    results++;
            }
            return results;
        }

        private bool isItemValid(KeyValuePair<ItemType, string> item)
        {
            /*
            byr(Birth Year) - four digits; at least 1920 and at most 2002.
            iyr(Issue Year) - four digits; at least 2010 and at most 2020.
            eyr(Expiration Year) - four digits; at least 2020 and at most 2030.
            hgt(Height) - a number followed by either cm or in:
            If cm, the number must be at least 150 and at most 193.
            If in, the number must be at least 59 and at most 76.
            hcl(Hair Color) - a # followed by exactly six characters 0-9 or a-f.
            ecl(Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
            pid(Passport ID) - a nine - digit number, including leading zeroes.
            cid(Country ID) - ignored, missing or not.
            */
            switch (item.Key)
            {
                case ItemType.byr:
                    return checkYearRange(item.Value, 1920, 2002);
                case ItemType.iyr:
                    return checkYearRange(item.Value, 2010, 2020);
                case ItemType.eyr:
                    return checkYearRange(item.Value, 2020, 2030);
                case ItemType.hgt:
                    if (item.Value.EndsWith("in"))
                        return checkHeightRange(item.Value.Replace("in", ""), 59, 76);
                    if (item.Value.EndsWith("cm"))
                        return checkHeightRange(item.Value.Replace("cm", ""), 150, 193);
                    return false;
                case ItemType.hcl:
                    if (item.Value.Length != 7)
                        return false;
                    var hclMatch = Regex.Match(item.Value, @"\#[a-f0-9]{6}");
                    return hclMatch.Success;
                case ItemType.ecl:
                    if (item.Value.Length != 3)
                        return false;
                    return Enum.TryParse(typeof(eyeColor), item.Value, out object result);
                case ItemType.pid:
                    if (item.Value.Length != 9)
                        return false;
                    var pidMatch = Regex.Match(item.Value, "[0-9]{1,9}");
                    return pidMatch.Success;
                case ItemType.cid:
                    return true;
                default:
                    return false;
            }

        }

        private bool checkYearRange(string value, int min, int max)
        {
            if (value.Length != 4)
                return false;
            if (Int16.TryParse(value, out var year))
            {
                return year >= min && year <= max;
            };
            return false;

        }

        private bool checkHeightRange(string value, int min, int max)
        {
            if (Int16.TryParse(value, out var height))
            {
                return height >= min && height <= max;
            };
            return false;

        }

        enum eyeColor
        {
            amb, blu, brn, gry, grn, hzl, oth
        }

        enum ItemType
        {
            byr,
            iyr,
            eyr,
            hgt,
            hcl,
            ecl,
            pid,
            cid


        }

    }
}
