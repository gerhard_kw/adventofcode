﻿using System;

namespace AdventOfCode
{
    class Program
    {
        static void Main(string[] args)
        {
            //var d1 = new Day1();
            //var r1 = d1.GetResult();
            //var s1 = d1.GetStar();
            //Console.WriteLine("Day 1:" + r1.ToString() + " // " + s1.ToString());

            //var d2 = new Day2();
            //var r2 = d2.GetResult();
            //var s2 = d2.GetStar();
            //Console.WriteLine("Day 2:" + r2.ToString() + " // " + s2.ToString());

            //var d3 = new Day3();
            //long s3 = d3.GetStar();
            //var r3 = d3.GetResult();
            //Console.WriteLine("Day 3:" + r3.ToString() + " // " + s3.ToString());

            // var d4 = new Day4();
            // long s4 = d4.GetStar();
            // var r4 = d4.GetResult();
            // Console.WriteLine("Day 4:" + r4.ToString() + " // " + s4.ToString());
            // Console.ReadLine();
            
            var d5 = new Day5();
            long s5 = d5.GetStar();
            var r5 = d5.GetResult();
            Console.WriteLine("Day 5:" + r5.ToString() + " // " + s5.ToString());
            Console.ReadLine();
        }

    }
}
