

using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace AdventOfCode
{
    class Day5
    {


        private List<string> dummy = new List<string>() {
            "BFFFBBFRRR", // : row 70, column 7, seat ID 567.
            "FFFBBBFRRR", //: row 14, column 7, seat ID 119.
            "BBFFBBFRLL"//  row 102, column 4, seat ID 820.
        };

        private List<string> GetLines()
        {
            string[] lines = File.ReadAllLines("Input/day5.txt", Encoding.UTF8);
            List<string> result = new List<string>();
            result.AddRange(lines);
            return result;
        }

        public int GetResult()
        {
            var result = 0;
            foreach (string str in GetLines())
            {
                var row = str.Substring(0, 7).Replace("F", "0").Replace("B", "1");
                var seat = str.Substring(7, 3).Replace("L", "0").Replace("R", "1");
                var tmp =  Convert.ToInt32(row,2) * 8 + Convert.ToInt32(seat,2);
                result = tmp > result ? tmp : result;
            }
            return result;

        }
        
        public int GetStar()
        {
            // It's a completely full flight, so your seat should be the only missing boarding pass in your list. 
            // However, there's a catch: some of the seats at the very front and back of the plane don't exist on
            // this aircraft, so they'll be missing from your list as well.
            // Your seat wasn't at the very front or back, though; the seats with IDs +1 and -1 from yours will be in your list.

            var allSeats = GetLines();

            for (int i = 65; i < 128; i++)
            {
                for (int j = 1; j < 8; j++)
                {
                    string seat = Transpose(Convert.ToString(i,2),false)
                         + Transpose(Convert.ToString(j,2),true);
                    Console.WriteLine("Seat {0} -  {1} / {2}", seat, i, j);
                    var current = (i*8+j);
                    
                    // TODO: check neighbours
                    if(!allSeats.Contains(seat) && hasNeighbours(current, allSeats))
                        return current;
                }
            }
            
            for (int i = 64; i > 1; i--)
            {
                for (int j = 7; j >= 0; j--)
                {
                    string seat = Transpose(Convert.ToString(i,2),false)
                         + Transpose(Convert.ToString(j,2),true);
                    Console.WriteLine("Seat {0} -  {1} / {2}", seat, i, j);
                    var current = (i*8+j);
                    
                    // TODO: check neighbours
                    if(!allSeats.Contains(seat) && hasNeighbours(current, allSeats))
                        return current;
                    

                }
            }      


            return 0;

        }

        private bool hasNeighbours(int seat, List<string> allSeats)
        {
            return (allSeats.Contains(getSeatCode(seat+1)) && 
                allSeats.Contains(getSeatCode(seat-1)));
        }

        private string getSeatCode(int seat){
            return Transpose(Convert.ToString(seat / 8,2),false)
                         + Transpose(Convert.ToString(seat % 8,2),true);
        }
        private string Transpose(string str, bool isSeat)
        {
            if(!isSeat) {

                while(str.Length<7)
                    str =  "F"+str;
                return str.Replace("0","F").Replace("1","B");
            }

            while(str.Length<3)
                str = "L" + str;
            return str.Replace("0","L").Replace("1","R");
        }
    }
}